import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Employee } from '../employee';

@Component({
  selector: 'add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  public message:string;
  public error:string;
  nameFormControl = new FormControl('', [Validators.required]);
  salaryFormControl = new FormControl('', [Validators.required]);
  ageFormControl = new FormControl('', [Validators.required]);
  
  constructor(private _employeeService:EmployeeService) { }

  onSubmit(name_employee:string, salary_employee:number, age_employee:number):void {
    if(this.nameFormControl.hasError('required') || this.salaryFormControl.hasError('required') || this.ageFormControl.hasError('required')) {
      this.error = "Please insert name, salary, and age.";
    }
    else {
      this._employeeService.addEmployee({name_employee, salary_employee, age_employee} as Employee)
      .subscribe(result => {
        this.message = "Employee has been added.";
      });
    }
  }

  ngOnInit(): void {  
    
  }
}
