import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { Employee } from '../employee';
import { ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {
  public message:string;
  public error:string;
  public employee:Employee;
  
  nameFormControl = new FormControl('', [Validators.required]);
  salaryFormControl = new FormControl('', [Validators.required]);
  ageFormControl = new FormControl('', [Validators.required]);
  
  constructor(private _employeeService:EmployeeService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    const id_employee = +this.route.snapshot.paramMap.get('id');
    this._employeeService.getEmployee(id_employee)
      .subscribe(result => {
        this.employee = result;
        this.nameFormControl.setValue(this.employee.name_employee);
        this.salaryFormControl.setValue(this.employee.salary_employee);
        this.ageFormControl.setValue(this.employee.age_employee);
      });
  }

  onSubmit(name_employee:string, salary_employee:number, age_employee:number):void {
    const id_employee = +this.route.snapshot.paramMap.get('id');
    if(this.nameFormControl.hasError('required') || this.salaryFormControl.hasError('required') || this.ageFormControl.hasError('required')) {
      this.error = "Please insert name, salary, and age.";
    }
    else {
      this._employeeService.editEmployee({id_employee, name_employee, salary_employee, age_employee} as Employee)
        .subscribe(result => {
          this.message = "Employee has been edited.";
        });
    }
  }
}
