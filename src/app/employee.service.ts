import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Employee } from './employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private _url:string = "http://localhost:8080/employee";

  constructor(private http:HttpClient) { }

  getEmployees():Observable<Employee[]> {
     const result:Observable<Employee[]> = this.http.get<Employee[]>(`${this._url}/getEmployees`);
     return result;
  }
  
  getEmployee(id:number):Observable<Employee> {
    return this.http.get<Employee>(`${this._url}/getEmployeeById/${id}`);
  }

  addEmployee(employee:Employee):Observable<Employee> {
    return this.http.post<Employee>(`${this._url}/addEmployee`, {
      "name_employee": `${employee.name_employee}`,
      "salary_employee": `${employee.salary_employee}`,
      "age_employee": `${employee.age_employee}`
    });
  }

  editEmployee(employee:Employee):Observable<Employee> {
    return this.http.put<Employee>(`${this._url}/editEmployee/${employee.id_employee}`, {
      "name_employee": `${employee.name_employee}`,
      "salary_employee": `${employee.salary_employee}`,
      "age_employee": `${employee.age_employee}`
    }); 
  }

  deleteEmployee(id:number):Observable<void> {
    return this.http.delete<void>(`${this._url}/deleteEmployee/${id}`);
  }
}

// export interface ResultEmployees {
//   status: string;
//   data: Employee[];
// }

// export interface ResultEmployee {
//   status: string;
//   data: Employee;
// }