import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'delete-employee',
  templateUrl: './delete-employee.component.html',
  styleUrls: ['./delete-employee.component.css']
})
export class DeleteEmployeeComponent implements OnInit {
  public message:string;
  constructor(private _employeeService:EmployeeService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    const id_employee = +this.route.snapshot.paramMap.get('id');
    this._employeeService.deleteEmployee(id_employee)
      .subscribe(() => {
        console.log('deleted');
        this.message = "Employee has been deleted."
      });
  }
}
