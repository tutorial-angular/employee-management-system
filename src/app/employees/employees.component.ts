import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, MatSortable } from '@angular/material/sort';

import { EmployeeService } from '../employee.service';
import { Employee } from '../employee';

@Component({
  selector: 'employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  displayedColumns:string[] = ['id_employee','name_employee','salary_employee','age_employee','actions'];
  dataSource = new MatTableDataSource<Employee>();
  public employees:Employee[];

  constructor(private _employeeService:EmployeeService) { }

  @ViewChild(MatSort, {static: true}) sort:MatSort;
  
  ngOnInit(): void {
    this._employeeService.getEmployees()
      .subscribe(results => {
        this.dataSource.data = results;
        this.sort.sort(({ id: 'id_employee', start: 'asc'}) as MatSortable);
        this.dataSource.sort = this.sort; 
      });
  }

  applyFilter(filterValue:string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.filterPredicate = (data:Employee, filter:string) => data.name_employee.toLowerCase().indexOf(filter) != -1;
  }
}
