import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';

import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {
  public employee:Employee;
  dataSource = new MatTableDataSource<Employee>();
  
  constructor(private _employeeService:EmployeeService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    const id_employee = +this.route.snapshot.paramMap.get('id');
    console.log(id_employee);
    this._employeeService.getEmployee(id_employee)
      .subscribe(result => this.employee = result as Employee);
  }
}